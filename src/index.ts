/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:36
 *
 */

import './styles/style.scss';
import Game from "./typescript/game";

// Init the game
const game = new Game();
game.init();
