/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

export class Utility {
    constructor() {
    }

    /*
        get Random int number
     */
    getRandomIntNumber(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /*
        get Random double number
    */
    getRandomDoubleNumber(min: number, max: number): number {
        return Number((Math.random() * (max - min) + min).toFixed(3))
    }
}
