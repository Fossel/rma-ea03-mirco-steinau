/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

import {Ammunition} from "./armor/ammunition";
import {Utility} from "./utility";

import {
    GAME_SOUND_EMPTY,
    GAME_SOUND_RELOAD,
    GAME_SOUND_SHOOT,
    GAME_STATE,
    GAME_TIME,
    MENU_BACKGROUND,
    RELOAD_POINTS,
    TARGET_FLYING_BAT,
    TARGET_FLYING_BAT_JSON,
    TARGET_FLYING_BEE,
    TARGET_FLYING_BEE_JSON,
    TARGET_FLYING_CLOUD,
    TARGET_FLYING_CLOUD_JSON,
    TARGET_FLYING_FLY,
    TARGET_FLYING_FLY_JSON,
    TARGET_GROUND_ALIEN_ROSA,
    TARGET_GROUND_ALIEN_ROSA_JSON,
    TARGET_GROUND_SPIDER,
    TARGET_GROUND_SPIDER_JSON,
    TILE_SCOREBOARD_CENTER,
    GAME_BACKGROUND
} from "./game.constants";
import {ImageRenderer} from "./renderer/image-renderer";
import {TextRenderer} from "./renderer/text-renderer";
import TargetFactory from "./targets/target-factory";

export default class Game {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private utility: Utility
    public armor: any[] = [];
    public armorCount: number = 10;
    public score: number = 0;
    private targets: any[] = [];
    private bgImg: any;
    private targetFactory: TargetFactory;
    private gameStatus = GAME_STATE.MENU;
    private menuBackground: any = null;
    private scoreboardBackground: any = null;
    private timeBackground: any = null;
    private retryBackground: any = null;
    private time: number = GAME_TIME;
    private timer: any = null;

    private gameMenuObjects: any = {
        alien: {
            image: null,
            text: null
        },
        bee: {
            image: null,
            text: null
        },
        bat: {
            image: null,
            text: null
        },
        cloud: {
            image: null,
            text: null
        },
        fly: {
            image: null,
            text: null
        },
        spider: {
            image: null,
            text: null
        }
    }

    private audios: { [key: string]: HTMLAudioElement } = {
        shoot: null,
        reload: null,
        empty: null
    }

    constructor() {
        this.utility = new Utility();
        this.targetFactory = new TargetFactory();
        this.createCanvas();
    }

    /*
        Init game
     */
    public init(): void {
        this.bgImg = new ImageRenderer(GAME_BACKGROUND, 0, 0, this.canvas.width, this.canvas.height - 80);
        this.retryBackground = new ImageRenderer(TILE_SCOREBOARD_CENTER, this.canvas.width / 2 - 150, this.canvas.height - 60, 300, 50);
        this.menuBackground = new ImageRenderer(MENU_BACKGROUND, 0, this.canvas.height - 80, this.canvas.width, 80);

        this.audios.shoot = new Audio(GAME_SOUND_SHOOT)
        this.audios.reload = new Audio(GAME_SOUND_RELOAD);
        this.audios.empty = new Audio(GAME_SOUND_EMPTY);

        this.initScoreboard();
        this.initTimer();
        this.initArmor();
        this.bindGameEvents();
        this.run();
    }

    /*
        create canvas
     */
    private createCanvas(): void {
        this.canvas = <HTMLCanvasElement>document.createElement('canvas');
        this.ctx = this.canvas.getContext("2d");
        this.canvas.width = innerWidth;
        this.canvas.height = innerHeight;

        document.getElementById('main').appendChild(this.canvas);
    }

    /*
        init scoreboard
     */
    private initScoreboard(): void {
        this.scoreboardBackground = new ImageRenderer(TILE_SCOREBOARD_CENTER, this.canvas.width / 2 - 300, this.canvas.height - 60, 300, 50);
        new TextRenderer(`Punkte: ${this.score}`, this.canvas.width / 2 - 100, this.canvas.height - 30).draw(this.ctx);
    }

    /*
        init timer
     */
    private initTimer(): void {
        this.timeBackground = new ImageRenderer(TILE_SCOREBOARD_CENTER, this.canvas.width / 2 + 10, this.canvas.height - 60, 300, 50);
        new TextRenderer(`Zeit: ${this.time}`, this.canvas.width / 2 - 100, this.canvas.width / 2 + 40).draw(this.ctx);
    }

    /*
        init targets
     */
    private initTargets(): void {
        for (let i = 0; i < this.utility.getRandomIntNumber(1, 2); i++) {
            let target = this.targetFactory.createTarget(this.utility.getRandomIntNumber(1, 6), this.canvas);
            target.draw(this.ctx);
            this.targets.push(target)
        }
    }

    /*
        init Armor
     */
    private initArmor(): void {
        this.armor = [];
        this.armorCount = 10;

        for (let i = 0; i < 10; i++) {
            let armor = new Ammunition(this.canvas);
            armor.draw(this.ctx, (i * 24));
            this.armor.push(armor);
        }
    }

    /*
      Bind gaming events
     */
    private bindGameEvents(): void {
        // Reload
        this.canvas.addEventListener('contextmenu', (evt) => { // Right click
            if (this.gameStatus === GAME_STATE.RUNNING) {
                evt.preventDefault();

                if (this.armorCount !== 10) {
                    this.initArmor();
                    this.audios.reload.play();
                    this.score -= RELOAD_POINTS;
                }
            }
        });
        this.canvas.addEventListener('click', (event: any) => {
            // Shooting

            if (event.button === 0 && this.gameStatus === GAME_STATE.RUNNING) {

                if (this.armorCount > 0) {
                    this.audios.shoot.pause();
                    this.audios.shoot.currentTime = 0;
                    this.audios.shoot.play();

                    this.armorCount--;
                    this.armor[this.armorCount].hide();

                    const canvasBounds = this.canvas.getBoundingClientRect();
                    const localX = event.pageX - canvasBounds.left;
                    const localY = event.pageY - canvasBounds.top;
                    let hitTargets = [];

                    for (let target of this.targets) {
                        if (target.isHit(localX, localY)) {
                            hitTargets.push({
                                index: this.targets.indexOf(target),
                                target: target
                            })
                        }
                    }
                    if (hitTargets.length > 0) {
                        const hitTarget = hitTargets.reduce(function (prev, current) {
                            return (prev.index > current.index) ? prev : current
                        });
                        this.targets.splice(hitTarget.index, 1);
                        this.score += hitTarget.target.getPoint();
                        if (this.score < 0) {
                            this.score = 0;
                        }
                    }
                } else {
                    this.audios.empty.play();
                }
            }
            // Start game button clicked
            if (event.button === 0 && this.gameStatus === GAME_STATE.MENU) {
                const canvasBounds = this.canvas.getBoundingClientRect();
                const localX = event.pageX - canvasBounds.left;
                const localY = event.pageY - canvasBounds.top;

                if (localX < this.canvas.width / 2 + 150 && localX > this.canvas.width / 2 - 150 && localY > this.canvas.height - 60 && localY < this.canvas.height + 40) {
                    this.gameStatus = GAME_STATE.RUNNING;
                    this.startTimer();
                    this.initTargets();
                }
            }

            // Retry game button clicked
            if (event.button === 0 && this.gameStatus === GAME_STATE.END) {
                const canvasBounds = this.canvas.getBoundingClientRect();
                const localX = event.pageX - canvasBounds.left;
                const localY = event.pageY - canvasBounds.top;

                if (localX < this.canvas.width / 2 + 150 && localX > this.canvas.width / 2 - 150 && localY > this.canvas.height - 60 && localY < this.canvas.height + 40) {
                    this.gameStatus = GAME_STATE.MENU;
                    this.time = GAME_TIME;
                    this.targets = [];
                    this.score = 0;
                }
            }
        });
    }

    /*
        Draw background
     */
    private renderBackground(): void {
        this.bgImg.draw(this.ctx);
        this.menuBackground.draw(this.ctx);
    }

    /*
        Game draw
     */
    gameDraw(): void {
        for (let i = 0; i < this.armor.length; i++) {
            this.armor[i].draw(this.ctx);
        }
        for (let target of this.targets) {
            target.move(this.ctx);
        }

        this.scoreboardBackground.draw(this.ctx);
        new TextRenderer(`Punkte: ${this.score}`, this.canvas.width / 2 - 250, this.canvas.height - 25).draw(this.ctx);

        this.timeBackground.draw(this.ctx);
        new TextRenderer(`Zeit: ${this.time}`, this.canvas.width / 2 + 40, this.canvas.height - 25).draw(this.ctx);
    }

    /*
        Draw game end screen
     */
    gameEnd(): void {
        this.ctx.fillStyle = "rgba(0,0,0, 0.6)";
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height - 80);

        this.ctx.fillStyle = "rgba(0,0,0, 0.9)";
        this.ctx.fillRect(this.canvas.width / 2 - this.canvas.width / 4, 64, this.canvas.width / 2, this.canvas.height / 2);

        this.menuBackground.draw(this.ctx);
        this.retryBackground.draw(this.ctx)
        new TextRenderer('Nochmal', this.canvas.width / 2 - 50, this.canvas.height - 30).draw(this.ctx);
        new TextRenderer(`${this.score} Punkte`, this.canvas.width / 2 - 80, this.canvas.height / 2, "rgba(255, 255, 255, 1)").draw(this.ctx);
        new TextRenderer(`Spielende`, this.canvas.width / 2 - 140, this.canvas.height / 4, "rgba(255, 255, 255, 1)", "700 60px Amatica SC, sans-serif").draw(this.ctx);
    }

    /*
        Draw game menu / info screen
     */
    gameMenu(): void {
        const left = this.canvas.width / 2 - this.canvas.width / 4;
        this.menuBackground.draw(this.ctx);
        this.retryBackground.draw(this.ctx);
        this.ctx.fillStyle = "rgba(0,0,0, 0.9)";
        this.ctx.fillRect(this.canvas.width / 2 - this.canvas.width / 4, 64, this.canvas.width / 2, this.canvas.height / 1.8);

        const height = 100;
        if (!this.gameMenuObjects.alien.image) {
            this.gameMenuObjects.alien.image = new ImageRenderer(TARGET_GROUND_ALIEN_ROSA, left + 20, height, TARGET_GROUND_ALIEN_ROSA_JSON.width, TARGET_GROUND_ALIEN_ROSA_JSON.height);
            this.gameMenuObjects.alien.text = new TextRenderer('- 1 Punkt', left + height + 20, height / 2 + TARGET_GROUND_ALIEN_ROSA_JSON.height, 'rgba(255,255,255,1');
        }

        if (!this.gameMenuObjects.fly.image) {

            this.gameMenuObjects.fly.image = new ImageRenderer(TARGET_FLYING_FLY, left + 20, 2 * height + 20, TARGET_FLYING_FLY_JSON.width, TARGET_FLYING_FLY_JSON.height);
            this.gameMenuObjects.fly.text = new TextRenderer('- 1 Punkt', left + height + 20, (2 * height) + TARGET_FLYING_FLY_JSON.height, 'rgba(255,255,255,1');
        }

        if (!this.gameMenuObjects.bat.image) {
            this.gameMenuObjects.bat.image = new ImageRenderer(TARGET_FLYING_BAT, left + 20, 6 * height - 120, TARGET_FLYING_BAT_JSON.width, TARGET_FLYING_BAT_JSON.height);
            this.gameMenuObjects.bat.text = new TextRenderer('+ 1 Punkt', left + height + 20, (6 * height) - 130 + TARGET_FLYING_BAT_JSON.height, 'rgba(255,255,255,1');
        }

        if (!this.gameMenuObjects.bee.image) {
            this.gameMenuObjects.bee.image = new ImageRenderer(TARGET_FLYING_BEE, left + 20, 5 * height - 80, TARGET_FLYING_BEE_JSON.width, TARGET_FLYING_BEE_JSON.height);
            this.gameMenuObjects.bee.text = new TextRenderer('+ 1 Punkt', left + height + 20, (5 * height) - 90 + TARGET_FLYING_BEE_JSON.height, 'rgba(255,255,255,1');
        }

        if (!this.gameMenuObjects.spider.image) {
            this.gameMenuObjects.spider.image = new ImageRenderer(TARGET_GROUND_SPIDER, left + 20, 4 * height - 50, TARGET_GROUND_SPIDER_JSON.width, TARGET_GROUND_SPIDER_JSON.height);
            this.gameMenuObjects.spider.text = new TextRenderer('+ 1 Punkt', left + height + 20, (4 * height) - 60 + TARGET_GROUND_SPIDER_JSON.height, 'rgba(255,255,255,1');
        }

        if (!this.gameMenuObjects.cloud.image) {
            this.gameMenuObjects.cloud.image = new ImageRenderer(TARGET_FLYING_CLOUD, left + 20, 3 * height - 10, TARGET_FLYING_CLOUD_JSON.width, TARGET_FLYING_CLOUD_JSON.height);
            this.gameMenuObjects.cloud.text = new TextRenderer('+ 1 Punkt', left + height + 20, (3 * height) - 20 + TARGET_FLYING_CLOUD_JSON.height, 'rgba(255,255,255,1');
        }

        this.gameMenuObjects.alien.image.draw(this.ctx);
        this.gameMenuObjects.alien.text.draw(this.ctx);

        this.gameMenuObjects.bee.image.draw(this.ctx);
        this.gameMenuObjects.bee.text.draw(this.ctx);

        this.gameMenuObjects.bat.image.draw(this.ctx);
        this.gameMenuObjects.bat.text.draw(this.ctx);

        this.gameMenuObjects.cloud.image.draw(this.ctx);
        this.gameMenuObjects.cloud.text.draw(this.ctx);

        this.gameMenuObjects.fly.image.draw(this.ctx);
        this.gameMenuObjects.fly.text.draw(this.ctx);

        this.gameMenuObjects.spider.image.draw(this.ctx);
        this.gameMenuObjects.spider.text.draw(this.ctx);

        new TextRenderer(`Nachladen ${RELOAD_POINTS} Punkt`, left * 2 + 20, this.canvas.height / 2, 'rgba(255,255,255,1').draw(this.ctx);
        new TextRenderer(`Zeit ${GAME_TIME}  Sekunden`, left * 2 + 20, this.canvas.height / 2 - 64, 'rgba(255,255,255,1').draw(this.ctx);
        new TextRenderer('Spielen', this.canvas.width / 2 - 50, this.canvas.height - 30).draw(this.ctx);
    }

    /*
        start timer
     */
    private startTimer(): void {
        this.timer = setInterval(() => {
            this.time--;
            this.initTargets();

            if (this.time <= 0) {
                clearInterval(this.timer);
                this.gameStatus = GAME_STATE.END;
            }
        }, 1000);
    }

    /*
        draw canvas
     */
    private run(): void {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.renderBackground();

        if (this.gameStatus === GAME_STATE.RUNNING
        ) {
            this.gameDraw();
        }
        if (this.gameStatus === GAME_STATE.END) {
            this.gameEnd();
        }
        if (this.gameStatus === GAME_STATE.MENU) {
            this.gameMenu();
        }
        window.setTimeout(() => window.requestAnimationFrame(this.run.bind(this)), 60);
    }
}
