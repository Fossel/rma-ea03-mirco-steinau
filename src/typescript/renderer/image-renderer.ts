/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

export class ImageRenderer {
    protected readonly image: HTMLImageElement;

    constructor(
        private img: string,
        private x: number,
        private y: number,
        private width: number,
        private height: number
    ) {
        this.image = new Image();
        this.image.src = this.img
    }

    /*
        render image on canvas
     */
    draw(ctx: CanvasRenderingContext2D): void {
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
}
