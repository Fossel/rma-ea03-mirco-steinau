/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

export class TextRenderer {

    constructor(
        private label: string,
        private x: number,
        private y: number,
        private color: string = 'rgba(0, 0, 0, 1)',
        private font: string = "500 25px Amatica SC, sans-serif"
    ) {
    }

    /*
        Render text on canvas
     */
    draw(ctx: CanvasRenderingContext2D): void {
        ctx.font = this.font;
        ctx.fillStyle = this.color;
        ctx.fillText(this.label, this.x, this.y);
    }
}
