/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

import {AMMUNITION_IMG} from "../game.constants";

export class Ammunition {
    protected readonly texture: HTMLImageElement;
    private right = 0;
    private canvas: any;

    constructor(
        private _canvas: any
    ) {
        this.canvas = _canvas;
        this.texture = new Image(40, 40);
        this.texture.src = AMMUNITION_IMG
    }

    /*
    draw one ammunition on canvas
     */
    draw(ctx: CanvasRenderingContext2D, _right: number = this.right): void {
        this.right = _right

        ctx.drawImage(
            this.texture,
            (this._canvas.width - this.right - 80),
            this.canvas.height - 50,
            this.texture.width,
            this.texture.height
        );
    }

    /*
        hide ammunition
     */
    hide(): void {
        this.texture.width = 40;
        this.texture.height = 0;
    }
}
