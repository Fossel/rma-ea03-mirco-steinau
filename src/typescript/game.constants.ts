/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

/*
* GLOBAL GAME SETTINGS
* */

import * as TARGET_CLOUD_JSON from '../../assets/images/targets/target-flying-cloud/target-flying-cloud.json';
import * as TARGET_ALIEN_ROSA_JSON from '../../assets/images/targets/target-alien-rosa/target-alien-rosa.json';
import * as TARGET_BEE_JSON from '../../assets/images/targets/target-bee/target-bee.json';
import * as TARGET_BAT_JSON from '../../assets/images/targets/target-bat/target-bat.json';
import * as TARGET_FLY_JSON from '../../assets/images/targets/target-fly/target-fly.json';
import * as TARGET_SPIDER_JSON from '../../assets/images/targets/target-spider/target-spider.json';

export const TARGET_FLYING_BEE: string = 'assets/images/targets/target-bee/bee.png';
export const TARGET_FLYING_BEE_IMG: string = 'assets/images/targets/target-bee/target_bee_sprite.png';
export const TARGET_FLYING_BEE_JSON: any = TARGET_BEE_JSON;

export const TARGET_FLYING_BAT: string = 'assets/images/targets/target-bat/bat.png';
export const TARGET_FLYING_BAT_IMG: string = 'assets/images/targets/target-bat/target-bat-sprite.png';
export const TARGET_FLYING_BAT_JSON: any = TARGET_BAT_JSON;

export const TARGET_FLYING_FLY: string = 'assets/images/targets/target-fly/fly.png';
export const TARGET_FLYING_FLY_IMG: string = 'assets/images/targets/target-fly/target-fly-sprite.png';
export const TARGET_FLYING_FLY_JSON: any = TARGET_FLY_JSON;

export const TARGET_FLYING_CLOUD: string = 'assets/images/targets/target-flying-cloud/cloud.png';
export const TARGET_FLYING_CLOUD_IMG: string = 'assets/images/targets/target-flying-cloud/target-flying-cloud-sprite.png';
export const TARGET_FLYING_CLOUD_JSON: any = TARGET_CLOUD_JSON;

export const TARGET_GROUND_SPIDER: string = 'assets/images/targets/target-spider/spider.png';
export const TARGET_GROUND_SPIDER_IMG: string = 'assets/images/targets/target-spider/target-spider-sprite.png';
export const TARGET_GROUND_SPIDER_JSON: any = TARGET_SPIDER_JSON;

//
export const TARGET_GROUND_ALIEN_ROSA: string = 'assets/images/targets/target-alien-rosa/p3_stand.png';
export const TARGET_GROUND_ALIEN_ROSA_IMG: string = 'assets/images/targets/target-alien-rosa/p3_walk.png';
export const TARGET_GROUND_ALIEN_ROSA_JSON: any = TARGET_ALIEN_ROSA_JSON;

export const AMMUNITION_IMG = 'assets/images/ammunition.svg';

export const MENU_BACKGROUND: string = 'assets/images/tiles/grassMid.png';
export const TILE_SCOREBOARD_CENTER: string = 'assets/images/tiles/chocoCenter_rounded.png';
export const GAME_BACKGROUND: string = 'assets/images/background.png';


export const GAME_STATE = {
    MENU: 0,
    RUNNING: 1,
    END: 2,
};
export const GAME_SOUND_SHOOT: string = 'assets/sounds/shoot.wav';
export const GAME_SOUND_EMPTY: string = 'assets/sounds/empty.wav';
export const GAME_SOUND_RELOAD: string = 'assets/sounds/reload.wav';


export const GAME_TIME: number = 30;
export const RELOAD_POINTS: number = -1;

