/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 */

import {Utility} from "../utility";

export class TargetModel {
    protected readonly image: HTMLImageElement;
    private animationIndex = 0;
    private utility: Utility;

    constructor(
        private canvas: HTMLCanvasElement,
        private src: string,
        private targetProps: any,
        private x: number,
        private y: number,
        private scale: number,
        private speed: number,
        private point: number,
    ) {
        this.utility = new Utility();
        this.image = new Image();
        this.image.src = this.src
    }

    /*
        Draw target
     */
    draw(ctx: CanvasRenderingContext2D): void {
        this.image.onload = () => {
            this.drawTarget(ctx);
        };
        this.drawTarget(ctx);
    }

    drawTarget(ctx: CanvasRenderingContext2D) {
        ctx.drawImage(
            this.image,
            this.targetProps.animation[this.animationIndex].x,
            this.targetProps.animation[this.animationIndex].y,
            this.targetProps.animation[this.animationIndex].width,
            this.targetProps.animation[this.animationIndex].height,
            this.x,
            this.y,
            this.targetProps.animation[this.animationIndex].width,
            this.targetProps.animation[this.animationIndex].height
        );
    }


    /*
        check if target was hit
     */
    isHit(mouseX: number, mouseY: number): boolean {
        return ((mouseX > this.x && mouseX < this.x + this.targetProps.animation[this.animationIndex].width) && (mouseY > this.y && mouseY < this.y + this.targetProps.animation[this.animationIndex].height))
    }

    /*
        move target from right to left
     */
    move(ctx: CanvasRenderingContext2D): void {
        this.x -= this.speed;
        (this.animationIndex + 1 < this.targetProps.animation.length) ? this.animationIndex++ : this.animationIndex = 0;
        this.draw(ctx);
    }

    /*
        get points of target
     */
    getPoint() {
        return this.point;
    }
}
