/*
 * Rich-Media-Anwendungen - EA03
 * Mirco Steinau
 *
 * 15.06.20, 13:37
 *
 */

import {TargetModel} from "./target.model";
import {Utility} from "../utility";
import {
    TARGET_FLYING_BAT_IMG,
    TARGET_FLYING_BAT_JSON,
    TARGET_FLYING_BEE_IMG,
    TARGET_FLYING_BEE_JSON,
    TARGET_FLYING_CLOUD_IMG,
    TARGET_FLYING_CLOUD_JSON,
    TARGET_FLYING_FLY_IMG,
    TARGET_FLYING_FLY_JSON,
    TARGET_GROUND_ALIEN_ROSA_IMG,
    TARGET_GROUND_ALIEN_ROSA_JSON,
    TARGET_GROUND_SPIDER_IMG,
    TARGET_GROUND_SPIDER_JSON
} from "../game.constants";

export default class TargetFactory {
    private utility: Utility;
    private canvas: HTMLCanvasElement;

    constructor() {
        this.utility = new Utility();
    }

    /*
        Generate targets
     */
    createTarget(target: number, canvas: HTMLCanvasElement): TargetModel {
        this.canvas = canvas;
        const targetOptions = {
            x: this.utility.getRandomIntNumber(canvas.width, canvas.width + 84),
            y: this.utility.getRandomIntNumber(1, canvas.height - 140),
            scale: this.utility.getRandomDoubleNumber(0.8, 1),
            speed: this.utility.getRandomDoubleNumber(1, 20)

        }
        switch (target) {
            case 1:
                return new TargetModel(
                    this.canvas,
                    TARGET_FLYING_BAT_IMG,
                    TARGET_FLYING_BAT_JSON,
                    targetOptions.x,
                    targetOptions.y,
                    targetOptions.scale,
                    targetOptions.speed,
                    1
                )
            case 2:
                return new TargetModel(
                    this.canvas,
                    TARGET_FLYING_CLOUD_IMG,
                    TARGET_FLYING_CLOUD_JSON,
                    targetOptions.x,
                    targetOptions.y,
                    targetOptions.scale,
                    targetOptions.speed,
                    1
                )

            case 3:
                return new TargetModel(
                    this.canvas,
                    TARGET_FLYING_BEE_IMG,
                    TARGET_FLYING_BEE_JSON,
                    targetOptions.x,
                    targetOptions.y,
                    targetOptions.scale,
                    targetOptions.speed,
                    1
                )
            case 4:
                return new TargetModel(
                    this.canvas,
                    TARGET_GROUND_SPIDER_IMG,
                    TARGET_GROUND_SPIDER_JSON,
                    targetOptions.x,
                    this.utility.getRandomIntNumber(canvas.height - (80 + TARGET_GROUND_SPIDER_JSON.height), canvas.height - 200),
                    targetOptions.scale,
                    targetOptions.speed,
                    1
                )
            case 5:
                return new TargetModel(
                    this.canvas,
                    TARGET_GROUND_ALIEN_ROSA_IMG,
                    TARGET_GROUND_ALIEN_ROSA_JSON,
                    targetOptions.x,
                    this.utility.getRandomIntNumber(canvas.height - (80 + TARGET_GROUND_ALIEN_ROSA_JSON.height), canvas.height - 200),
                    targetOptions.scale,
                    targetOptions.speed,
                    -1
                )
            case 6:
                return new TargetModel(
                    this.canvas,
                    TARGET_FLYING_FLY_IMG,
                    TARGET_FLYING_FLY_JSON,
                    targetOptions.x,
                    targetOptions.y,
                    targetOptions.scale,
                    targetOptions.speed,
                    -1
                )
        }
    }
}
