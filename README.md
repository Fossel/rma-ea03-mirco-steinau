# RMA - Mirco Steinau - EA03

# Start 
to load all requirements <code>npm install</code>


<code>npm run build:dev</code>
    starts dev server on <code>localhost:8080</code> with livereload, sourcemap
    
<code>npm run build:prod</code>
    creates prod files to <code>/dist</code>
